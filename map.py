import pygame
from numba.core import types
from numba.typed import Dict
from numba import int32
from pygame import Vector2

from settings import *

_ = False


text_map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, 1, 1, 1, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1, _, 1],
    [1, _, _, _, _, 2, _, _, _, _, _, _, _, _, _, 1, 1, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, 1, 1, 1, _, _, _, _, _, 2, _, _, _, _, _, _, _, 1],
    [1, _, _, 2, 1, _, _, _, _, 1, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, 1, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, _, 2, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, _, _, 2, _, _, _, 1, _, 1, _, _, _, _, _, _, _, 1],
    [1, _, _, 1, _, _, _, 2, _, 1, _, _, _, _, _, _, _, 1],
    [1, _, _, 1, _, _, _, 1, _, 1, _, _, _, _, _, _, _, 1],
    [1, 2, _, 1, _, _, _, 1, _, 2, 1, 1, 2, 1, _, 1, 1, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, 1, _, 1, _, _, _, _, _, _, 1],
    [1, 1, 1, 1, 2, 1, 1, 1, 1, _, 1, 1, 2, 1, 1, 2, 1, 1],
    [1, _, _, _, _, _, _, _, _, _, 1, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, 1, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, 2, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, 2, _, 1, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, 1, 1, 1, _, _, _, _, _, _, _, _, _, 1],
    [1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1],

]

WORLD_WIDTH = len(text_map[0]) * TILE
WORLD_HEIGHT = len(text_map) * TILE

map = Dict.empty(key_type=types.UniTuple(int32, 2), value_type=int32)
mini_map = set()

collisions = []

for j, row in enumerate(text_map):
    for i, char in enumerate(row):
        if char:
            mini_map.add((i * MAP_TILE, j * MAP_TILE))
            collisions.append(pygame.Rect(i * TILE, j * TILE, TILE, TILE))
            if char == 1:
                map[(i * TILE, j * TILE)] = 1
            elif char == 2:
                map[(i * TILE, j * TILE)] = 2


def map_size():
    return Vector2(len(text_map[0]) * TILE // MAP_SCALE, len(text_map) * TILE // MAP_SCALE)
