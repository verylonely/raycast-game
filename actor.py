from pygame import Vector2, Rect
from map import collisions

class Actor:

    def __init__(self, pos, speed, angle=0):
        self.pos: Vector2 = pos
        self.angle = angle
        self.speed = speed
        self.side = 50
        self.rect = Rect(*pos, self.side, self.side)

        self.collision_list = collisions


    def detect_collision(self, dx, dy):
        next_rect = self.rect.copy()
        next_rect.move_ip(dx, dy)
        hit_indexes = next_rect.collidelistall(collisions)

        if len(hit_indexes):
            delta_x, delta_y = 0, 0
            for hit_index in hit_indexes:
                hit_rect = self.collision_list[hit_index]
                if dx > 0:
                    delta_x += next_rect.right - hit_rect.left
                else:
                    delta_x += hit_rect.right - next_rect.left
                if dy > 0:
                    delta_y += next_rect.bottom - hit_rect.top
                else:
                    delta_y += hit_rect.bottom - next_rect.top

            if abs(delta_x - delta_y) < 10:
                dx, dy = 0, 0
            elif delta_x > delta_y:
                dy = 0
            elif delta_y > delta_x:
                dx = 0

        self.pos.x += dx
        self.pos.y += dy
