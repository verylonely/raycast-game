import math

import pygame
from settings import *
from collections import deque
from pygame import Vector2

class Sprites:
    def __init__(self):
        self.sprites_params = {
            'skull' : {
                'sprite': pygame.image.load('sprites/skull/base/skull.png').convert_alpha(),
                'angle': None,
                'shift': 3,
                'scale': 0.3,
                'animation': None
            },
            'campfire': {
                'sprite': pygame.image.load('sprites/campfire/base/0.png').convert_alpha(),
                'angle': None,
                'shift': 1.5,
                'scale': 0.8,
                'animation': deque(
                    [pygame.image.load(f'sprites/campfire/anim/{i}.png').convert_alpha() for i in range(5)]
                ),
                'animation_dist': 900,
                'animation_speed': 5
            },
            'room': {
                'sprite': pygame.image.load('sprites/room/base/0.png').convert_alpha(),
                'angle': None,
                'shift': 1.0,
                'scale': 1,
                'animation': None
            }
        }

        self.list_objects = [
            SpriteObject(self.sprites_params['skull'], Vector2(6,2)),
            SpriteObject(self.sprites_params['campfire'], Vector2(8, 4)),
            SpriteObject(self.sprites_params['skull'], Vector2(2, 12)),
            SpriteObject(self.sprites_params['skull'], Vector2(6,27))
        ]


class SpriteObject:
    def __init__(self, params, pos):
        self.object = params['sprite']
        self.angle = params['angle']
        self.pos: pygame.Vector2 = pos*TILE
        self.shift = params['shift']
        self.scale = params['scale']
        self.animation = params['animation']
        if self.animation:
            self.animation_speed = params['animation_speed']
            self.animation_dist = params['animation_dist']
            self.animation_count = 0


    def object_locate(self, player, walls):
        dx, dy = self.pos.x - player.pos.x, self.pos.y - player.pos.y
        distance_to_sprite = math.sqrt(dx ** 2 + dy ** 2)

        theta = math.atan2(dy, dx)
        gamma = theta - player.angle

        if dx > 0 and 180 <= math.degrees(player.angle) <= 360 or dx < 0 and dy < 0:
            gamma += math.tau

        delta_rays = int(gamma / DELTA_ANGLE)
        current_ray = int(CENTER_RAY + delta_rays)
        distance_to_sprite *= math.cos(HALF_FOV - current_ray * DELTA_ANGLE)

        fake_ray = current_ray + FAKE_RAYS
        if 0 <= fake_ray <= FAKE_RAYS_RANGE - 1 and distance_to_sprite > 30:
            proj_height = min(int(PROJ_COEFF / distance_to_sprite * self.scale), DOUBLE_HEIGHT)
            half_proj_height = proj_height // 2

            shift = half_proj_height * self.shift

            if self.animation and self.animation_dist > distance_to_sprite:
                sprite_obj = self.animation[0]
                if self.animation_count < self.animation_speed:
                    self.animation_count += 1
                else:
                    self.animation.rotate()
                    self.animation_count = 0
            else:
                sprite_obj = self.object
            sprite_pos = (current_ray * SCALE - half_proj_height, HALF_HEIGHT - half_proj_height + shift)
            sprite = pygame.transform.scale(sprite_obj, (proj_height, proj_height))
            return (distance_to_sprite, sprite, sprite_pos)
        else:
            return (False,)