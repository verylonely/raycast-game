import pygame
from settings import *
from raycasting import raycast
from map import mini_map

class Drawing:
    def __init__(self, sc, sc_map):
        self.sc = sc
        self.sc_map = sc_map
        self.font = pygame.font.SysFont('Arial', 36, bold=True)
        self.textures = {
            1: pygame.image.load("textures/brick_wall0.png").convert(),
            2: pygame.image.load("textures/brick_wall0_broken.png").convert(),
            0: pygame.image.load('textures/black.jpg').convert()
        }

    def background(self):
        pygame.draw.rect(self.sc, DARK_GRAY, (0, HALF_HEIGHT, WIDTH, HALF_HEIGHT))

    def world(self, world_objects):
        for obj in sorted(world_objects, key=lambda n: n[0], reverse=True):
            if obj[0]:
                _, object, object_pos = obj
                self.sc.blit(object, object_pos)
        # raycast(self.sc, player_pos, player_angle, self.textures)

    def fps(self, clock):
        fps = str(int(clock.get_fps()))
        render = self.font.render(fps, 0, YELLOW)
        self.sc.blit(render, FPS_POS)

    def draw_mini_map(self, player_pos, player_angle):
        self.sc_map.fill(BLACK)
        x, y = player_pos.x // MAP_SCALE, player_pos.y // MAP_SCALE
        pygame.draw.circle(self.sc_map, RED, (x,y), 3)

        for x, y in mini_map:
            pygame.draw.rect(self.sc_map, WHITE, (x, y, MAP_TILE, MAP_TILE))
        self.sc.blit(self.sc_map, MAP_POS)

        render = self.font.render("{}, {}".format(int(player_pos.x  / TILE), int(player_pos.y  / TILE)), 0, YELLOW)
        self.sc.blit(render, (10, MAP_POS[1] - 30))