import settings
from actor import Actor
import pygame
import math
from settings import HALF_WIDTH, HALF_HEIGHT

class Player(Actor):


    def movement(self):
        self.keys()
        self.angle %= math.tau
        self.mouse()
        self.rect.center = self.pos.x, self.pos.y

    def keys(self):
        sin_a = math.sin(self.angle)
        cos_a = math.cos(self.angle)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
            dx = self.speed * cos_a
            dy = self.speed * sin_a
            self.detect_collision(dx, dy)
        if keys[pygame.K_s]:
            dx = -self.speed * cos_a
            dy = -self.speed * sin_a
            self.detect_collision(dx, dy)
        if keys[pygame.K_a]:
            dx = self.speed * sin_a
            dy = -self.speed * cos_a
            self.detect_collision(dx, dy)
        if keys[pygame.K_d]:
            dx = -self.speed * sin_a
            dy = self.speed * cos_a
            self.detect_collision(dx, dy)

    def mouse(self):
        if pygame.mouse.get_focused():
            diff = pygame.mouse.get_pos()[0] - HALF_WIDTH
            pygame.mouse.set_pos((HALF_WIDTH, HALF_HEIGHT))
            self.angle += diff * 0.005
            pygame.mouse.set_visible(False)