import pygame
from pygame import RESIZABLE

from drawing import Drawing
from player import Player
from settings import *
import map

from sprites_objects import *
from raycasting import ray_casting_walls

pygame.init()
pygame.display.set_caption("Castle Of The Rays")

sc = pygame.display.set_mode((WIDTH, HEIGHT))

sc_map = pygame.Surface((map.map_size().x, map.map_size().y))

sprites = Sprites()

clock = pygame.time.Clock()
player = Player(pygame.Vector2(10, 2) * TILE, 2, 1.8)
drawing = Drawing(sc, sc_map)

pygame.mixer.init()
pygame.mixer.music.load('sounds/music/ambient.mp3')
pygame.mixer.music.play()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT or pygame.key.get_pressed()[pygame.K_ESCAPE]:
            exit()
    player.movement()
    sc.fill(DARKEST_GRAY)

    drawing.background()
    walls = ray_casting_walls(player, drawing.textures)
    drawing.world(walls + [obj.object_locate(player, walls) for obj in sprites.list_objects])
    #drawing.draw_mini_map(player.pos, player.angle)
    drawing.fps(clock)


    pygame.display.flip()
    clock.tick(FPS)



